#!/usr/bin/env node

var express = require('express');
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var app = express();
var port = 2000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use('/api', require('./routes/index'));


app.use(function (req, res) {
	res.sendFile(__dirname + '/public/dist/html/index.html');
});

var server = http.Server(app);
server.listen(port, function () {
	console.log('Servidor [entrevista] inicializado na porta ' + port);
});

var shutdown = function() {
	server.close(function() {
		console.log('Servidor [docs] finalizado!');
		process.exit();
	});
};

//process.on('SIGTERM', shutdown);
//process.on('SIGINT', shutdown);
