var gulp  = require('gulp');
var watch = require('gulp-watch');
var gulpif = require('gulp-if');
var concat = require('gulp-concat');
var less = require('gulp-less');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var sourcemaps = require('gulp-sourcemaps');
var htmlmin = require('gulp-htmlmin');
var htmlhint = require("gulp-htmlhint");
var hintConfig = {
	"doctype-first": false
};
var jsonminify = require('gulp-jsonminify');
var pump = require('pump');
var development = true;

// Operação padrão: chame 'gulp' no terminal
gulp.task('init', ['build-app', 'build-less', 'build-html', 'build-json', 'build-fonts']);

// Operação padrão: chame 'gulp' no terminal
gulp.task('default', ['init', 'watch']);

// Operação de produção
gulp.task('deploy', ['dev-false', 'init'], function(){
	development = true;
});
gulp.task('dev-false', function(){
	development = false;
});

// Constroi 1 único arquivo app.js
gulp.task('build-app', function(cb) {
	pump([
		gulp.src('./src/**/*.js'),
		gulpif(development, sourcemaps.init()),
		concat('app.js'),
		uglify(),
		gulpif(development, sourcemaps.write()),
		gulp.dest('./public/dist/js')
	], cb);
});

// Constroi 1 único arquivo .less
gulp.task('build-less', function(cb){
	pump([
		gulp.src('./src/**/*.less'),
		gulpif(development, sourcemaps.init()),
		less(),
		concat('styles.css'),
		uglifycss(),
		gulpif(development, sourcemaps.write()),
		gulp.dest('./public/dist/css')
	], cb);
});

// Constroi 1 único arquivo fonts
gulp.task('build-fonts', function(cb){
	pump([
		gulp.src('./src/**/*.fonts'),
		gulpif(development, sourcemaps.init()),
		less(),
		concat('fonts.css'),
		uglifycss(),
		gulpif(development, sourcemaps.write()),
		gulp.dest('./public/dist/css')
	], cb);
});

// Realoca arquivos html
gulp.task('build-html', function(cb){
	pump([
		gulp.src('./src/**/*.html'),
		htmlhint(hintConfig),
		htmlhint.reporter(),
		gulpif(!development, htmlmin({collapseWhitespace: true})),
		gulp.dest('./public/dist/html')
	], cb);
});

// Realoca arquivos json
gulp.task('build-json', function(cb){
	pump([
		gulp.src('./src/**/*.json'),
		gulpif(!development, jsonminify()),
		gulp.dest('./public/dist/html')
	], cb);
});

// Observa por mudanças nos arquivos
gulp.task('watch', function() {
	watch('./gulpfile.js', function(){
		gulp.start('init');
	});
	watch('./src/**/*.js', function(){
		gulp.start('build-app');
	});
	watch('./src/**/*.less', function(){
		gulp.start('build-less');
	});
	watch('./src/**/*.html', function(){
		gulp.start('build-html');
	});
	watch('./src/**/*.json', function(){
		gulp.start('build-json');
	});
	watch('./src/**/*.fonts', function(){
		gulp.start('build-fonts');
	});
});
