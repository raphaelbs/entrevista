var express = require('express');
var router = express.Router();

// Categorias
var categorias = [
	{id: 1, descricao: 'Anamnese', cor: '#a1f1f1', situacao: 1},
	{id: 2, descricao: 'Evolução', cor: '#f2a2f2', situacao: 1},
	{id: 3, descricao: 'Queixa/Evento', cor: '#f3f3a3', situacao: 1},
	{id: 4, descricao: 'Exame Físico', cor: '#44f4f4', situacao: 1},
	{id: 5, descricao: 'Diagnóstico', cor: '#f545f5', situacao: 1},
	{id: 6, descricao: 'Solicitação de exame', cor: '#f6f646', situacao: 1},
	{id: 7, descricao: 'Receita', cor: '#d7f7f7', situacao: 1},
	{id: 8, descricao: 'Atestado', cor: '#f8d8f8', situacao: 1},
	{id: 9, descricao: 'Laudo', cor: '#f9f9d9', situacao: 1},
];

/**
 * [/api/categorias]
 * GET
 * Lista as categorias.
 */
router.get('/categorias', function(req, res){
	return res.json(categorias);
});

/**
 * [/api/categorias]
 * POST
 * Insere nas categorias.
 */
router.post('/categorias', function(req, res){
	if(req.body.categoria) {
		for(var i=0; i<categorias.length; i++){
			if(req.body.categoria.id !== categorias[i].id) continue;
			categorias[index] = req.body.categoria;
			return res.json(categorias);
		}
		categorias.push(req.body.categoria);
	}
	return res.json(categorias);
});

/**
 * [/api/categorias]
 * POST
 * Insere nas categorias.
 */
router.delete('/categorias/:id', function(req, res){
	if(req.params.id){
		categorias.forEach(function(categoria, index){
			if(req.params.id !== categoria.id) return;
			categorias.splice(index, 1);
		});
	}
	return res.json(categorias);
});


module.exports = router;
