
angular.module('app', ['ui.router', 'ngMaterial', 'ngMessages']);

/**
* Script principal (inicial) do Bookclass.
* Criado por Gustavo em 21/07/2016.
*/
angular.module('app').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$mdThemingProvider', '$compileProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $compileProvider, $httpProvider) {

	// Desabilita modo de debug
	$compileProvider.debugInfoEnabled(false);
	// Desabilita compilador de diretiva do tipo Comentario
	$compileProvider.commentDirectivesEnabled(false);
	// Desabilita compilador de diretiva do tipo Class
	$compileProvider.cssClassDirectivesEnabled(false);
	// Agrupa chamadas Http para otimizar o $digest
	$httpProvider.useApplyAsync(true);

	// Configura o tema padrão
	$mdThemingProvider.theme('default')
	.primaryPalette('indigo')
	.accentPalette('amber');

	// Habilita cor para o Browser
	$mdThemingProvider.enableBrowserColor({
		theme: 'default',
		palette: 'indigo',
		hue: '300'
	});

	// Habilita html5 no ui-router
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});

	// Rota padrão
	$urlRouterProvider.otherwise('/');

	// States:
	$stateProvider
	.state('home', {
		url: '/',
		views : {
			'principal' : {
				templateUrl : '/dist/html/home/home.html'
			},
			'menu' : {
				templateUrl : '/dist/html/menu/menu.html'
			}
		}
	})
	.state('prontuario', {
		url: '/prontuario',
		views : {
			'principal' : {
				templateUrl : '/dist/html/prontuario/prontuario.html',
				controller : 'prontuario'
			},
			'menu' : {
				templateUrl : '/dist/html/menu/menu.html'
			}
		}
	})
	.state('categoria', {
		url: '/categoria',
		views : {
			'principal' : {
				templateUrl : '/dist/html/categoria/categoria.html',
				controller : 'categoria'
			},
			'menu' : {
				templateUrl : '/dist/html/menu/menu.html'
			}
		}
	});
}]);
