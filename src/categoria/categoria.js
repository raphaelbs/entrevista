/**
 * [prontuario]
 * Controler para tela principal.
 */
angular.module('app').controller('categoria', ['$scope', '$http', function($scope, $http){
	$http.get('/api/categorias')
	.then(function(resp){
		$scope.categorias = resp.data;
	});

	$scope.categoria = {};
	// situações
	$scope.situacoes = [
		{ value: 1, label: "Visível" },
		{ value: 2, label: "Oculta" }
	];
	// classificações
	$scope.classificacoes = [
		{ value: 1, label: "Texto" },
		{ value: 2, label: "Formulário" }
	];

	$scope.novo = function(descricao){
		$scope.categoria = { descricao : descricao };
		$scope.tabPrincipal = 1;
	};

	$scope.editar = function(categoria){
		$scope.categoria = categoria;
		$scope.tabPrincipal = 1;
	};

	$scope.salvar = function(categoria){
		$http.post('/api/categorias', {categoria: categoria})
		.then(function(resp){
			$scope.categorias = resp.data;
		});
	};

	$scope.deletar = function(categoria){
		$http.delete('/api/categorias/' + categoria.id)
		.then(function(resp){
			$scope.categorias = resp.data;
		});
	};
}])
